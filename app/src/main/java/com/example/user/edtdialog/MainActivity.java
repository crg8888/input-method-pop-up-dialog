package com.example.user.edtdialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Dialog mDialog;
    private Button Btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Btn = findViewById(R.id.Btn);
        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog = new Dialog(MainActivity.this, R.style.BottomDialog);
                mDialog.setCanceledOnTouchOutside(false);
                mDialog.show();
                Window window = mDialog.getWindow();
                window.setContentView(R.layout.eva_dialog);
                WindowManager.LayoutParams pl = window.getAttributes();
                pl.gravity = Gravity.BOTTOM;
                pl.width = WindowManager.LayoutParams.MATCH_PARENT;
                pl.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.getDecorView().setPadding(0, 0, 0, 0);
                window.setAttributes(pl);
                final EditText commentEditText = (EditText) mDialog.findViewById(R.id.EdtText);
                commentEditText.setHint("回复");
                TextView sendButton = (TextView) mDialog.findViewById(R.id.textBtn);
                TextView textClear = mDialog.findViewById(R.id.textClear);
                mDialog.show();
                commentEditText.setFocusable(true);
                commentEditText.setFocusableInTouchMode(true);
                commentEditText.requestFocus();//获取焦点 光标出现
                InputMethodManager imm = (InputMethodManager) MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                textClear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        commentEditText.setText("");
                        InputMethodManager imm = (InputMethodManager) MainActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0); //强制隐藏键盘
                        mDialog.dismiss();
                    }
                });
                sendButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
            }
        });
    }
}
